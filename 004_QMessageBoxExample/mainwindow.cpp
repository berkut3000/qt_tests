#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
#if 0
    QMessageBox::about(this, "My Title", "This is my Custom Message");
#endif
#if 0
    QMessageBox::aboutQt(this, "My Title");
#endif
#if 0
    QMessageBox::critical(this, "My Title", "This is my Custom Message");
#endif
#if 0
    QMessageBox::information(this, "My Title", "This is my Custom Message");
#endif
#if 0
    QMessageBox::question(this, "My Title", "This is my Custom Message");
#endif
#if 0
    QMessageBox::warning(this, "My Title", "This is my Custom Message");
#endif
#if 0
    QMessageBox::question(this, "My Title", "This is my Custom Message",QMessageBox::Yes | QMessageBox::No);
#endif
#if 1
    QMessageBox::StandardButton reply =
            QMessageBox::question(this, "My Title", "This is my Custom Message");
    if(reply == QMessageBox::Yes)
    {
        QApplication::quit();
    } else {
        qDebug() << "No pasa nada";
    }

#endif

}
