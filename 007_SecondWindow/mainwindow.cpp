#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
#if 0
    SecDialog secDialog;
    secDialog.setModal(true);
    secDialog.exec();
#endif
    hide();
    secDialog = new SecDialog(this);
    secDialog->show();
}
